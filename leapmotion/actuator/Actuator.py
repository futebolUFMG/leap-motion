# New File
import socket
import time

from beautifulhue.api import Bridge
import connection
# Atuador new

class SettingLamp():
    def __init__(self):
        self.lamp = 1
        self.color = 5000
        self.status = False
        self.clock = "C"
        self.resultG = 0
        self.bri = 70

    def setLamp(self, lamp):
        self.lamp = lamp

    def getLamp(self):
        return self.lamp


    def setStatusLamp(self, statusLamp):
       self.status = statusLamp
       self.update_lamp()

    def getStatusLamp(self):
        return self.status

    def setColor(self, color):
        self.color = color


    def getColor(self):
        return self.color

    def setBri(self, bri):
        self.bri= bri

    def getBri(self):
        return self.bri

    def setClock(self, clock):
        self.clock = clock

    def getClock(self):
        return self.clock

    def getResultG(self):
        return self.resultG


    # Alterar IP do HUE
    def update_lamp(self):
        bridge = Bridge(device={'ip': '192.168.0.91'}, user={'name': 'tnjsYZX9QjU0EPtMHlspQjNA40g4547G8RX80gik'})
        resource = {
            'which': self.getLamp(),
            'data': {
                'state': {'on': self.status, 'hue': self.getColor(), 'bri': self.getBri(),'sat' : 254}
            }
        }
        bridge.light.update(resource)

    # Alterar IP do HUE
    def update_lamp_all(self, status_lamp):
        bridge = Bridge(device={'ip': '192.168.0.91'}, user={'name': 'tnjsYZX9QjU0EPtMHlspQjNA40g4547G8RX80gik'})
        resource_1 = {
            'which': 1,
            'data': {
                'state': {'on': status_lamp, 'hue': 50000, 'bri':self.getBri(), 'sat': 254}
            }
        }

        resource_2 = {
            'which': 2,
            'data': {
                'state': {'on': status_lamp, 'hue': 50000, 'bri':self.getBri(), 'sat': 254}
            }
        }

        resource_3 = {
            'which': 3,
            'data': {
                'state': {'on': status_lamp, 'hue': 50000, 'bri': self.getBri(), 'sat': 254}
            }
        }


        bridge.light.update(resource_1)
        bridge.light.update(resource_2)
        bridge.light.update(resource_3)

def main():
    lamp = SettingLamp()

    # Conexao com a VM Bristol / Vm Server / Vm Local
    s = connection.Client('137.222.204.77', 5005)
    s.connect()

    # Conexao com o Sensor
    # Alterar o IP do Sensor
    a = connection.Client('192.168.0.39', 59000)
    a.connect()

    try:
        while True:
            #a.send_message(dados)
            result = s.receive_message()
            resultsplit = result.split()
            try:
                dados = resultsplit[0]
                time_recv = resultsplit[1]
                palm = resultsplit[2]
                time_proc_recv = resultsplit[3]
                print dados
                print "Time proce : " + time_proc_recv
                palmsplit = palm.split(".")
            except IndexError:
                print "Index Error"



            if (dados == "on"):
                print "On"
                lamp.setStatusLamp(True)
            elif (dados == "off"):
                print "Off"
                lamp.setStatusLamp(False)
            elif(dados == "1"):
                print "Lamp 1"
                lamp.setLamp(1)
                lamp.update_lamp()
            elif (dados == "2"):
                print "Lamp 2"
                lamp.setLamp(2)
                lamp.update_lamp()
            elif (dados == "3"):
                print "Lamp 3"
                lamp.setLamp(3)
                lamp.update_lamp()
            elif (dados == "allon"):
                print dados
                lamp.update_lamp_all(True)
            elif (dados == "alloff"):
                print dados
                lamp.update_lamp_all(False)
            elif (dados == "D"):
                 
                 newColor = (lamp.getColor() + 10000) % 60000
                 lamp.setColor(newColor)
                 lamp.update_lamp()
            elif (dados == "E"):
                 print "Color : "
                 newColor = (lamp.getColor() - 10000) % 60000
                 lamp.setColor(newColor)
                 lamp.update_lamp()

            try:
                p = palmsplit[0]
                pint = int(p)
                lamp.setBri(pint)
            except ValueError:
                    print "Erro Value Error"

            print "Time Recv : " + time_recv
            time_command = str(time_recv) + " " + str(dados) + " " + str(time_proc_recv) + " "
            a.send_message(time_command)



    except KeyboardInterrupt:
        s.destroy()
        a.destroy()

if __name__ == '__main__':

    main()
