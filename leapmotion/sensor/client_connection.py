import socket
import os

import logging


class Connection(object):
    def __init__(self, HOST , PORT):
        while True:
            print " OPENING SOCKET"
            try:
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.HOST = HOST
                self.PORT = PORT
                self.connected = False
                self.log = logging.getLogger("vc_logger")
                break
            except socket.error as e:
                print "Socket Erro"
            except socket.herror as e:
                print "Socket Erro"
            except socket.gaierror as e:
                print "Socket Erro"
            except socket.timeout as e:
                print "Socket Erro Time"
            except Exception as e:
                print "Erro Excpetion"


    def destroy(self):
        self.s.close()



class Client(Connection):

    def connect(self):
        attempts = 0
        while True:
            try:
                print "CONNECTION TO: " + self.HOST + " ON PORT:" + str(self.PORT)
                self.s.connect((self.HOST, self.PORT))
                self.connected = True
                break
            except socket.error as e:
                print "Socket Erro"
                self.log.debug('SOCKET ERROR: ' + str(e))
            except socket.herror as e:
                print "Socket Erro"
                self.log.debug('SOCKET HERROR: ' + str(e))
            except socket.gaierror as e:
                print "Socket Erro"
                self.log.debug('SOCKET GAIERROR: ' + str(e))
            except socket.timeout as e:
                print "Socket Erro"
                self.log.debug('SOCKET TIMEOUT: ' + str(e))
            except Exception as e:
                print "Socket Erro"
                self.log.debug('ERROR CONNECTING SOCKET: ' + str(e))
                self.log.debug('Attempts: ' + str(attempts))
            attempts += 1

    def send_message(self, message=''):

        try:
            self.s.send(message)

        except IOError as e:
            print "Socket Erro 1"
            self.log.debug('ERROR: ' + str(e))
        except socket.error as e:
            print "Socket Erro 2"
            self.log.debug('SOCKET ERROR: ' + str(e))
        except socket.herror as e:
            print "Socket Erro 3"
            self.log.debug('SOCKET HERROR: ' + str(e))
        except socket.gaierror as e:
            print "Socket Erro 4"
            self.log.debug('SOCKET GAIERROR: ' + str(e))
        except socket.timeout as e:
            print "Socket Erro 5"
            self.log.debug('SOCKET TIMEOUT: ' + str(e))
        except KeyboardInterrupt:
            print "Socket Erro 6"
            self.log.debug('Finished')
        else:
            print "Send...."
            self.log.debug('Finished sending')

    def receive_message(self, nbytes = 1024):
        try:

            data = self.s.recv(nbytes)
            if not data:
                self.destroy()
            return data
        except socket.error as e:
            print "Socket Erro"
            self.log.debug('SOCKET ERROR: ' + str(e))
        except socket.herror as e:
            print 'SOCKET HERROR: ' + str(e)
        except socket.gaierror as e:
            print "Socket Erro"
            self.log.debug('SOCKET GAIERROR: ' + str(e))
        except socket.timeout as e:
            print "Socket Erro"
            self.log.debug('SOCKET TIMEOUT: ' + str(e))
        except Exception as e:
            print "Socket Erro"
            self.log.debug('ERROR: error receiving message')
        else:
            self.log.info('Finished sending')

    def destroy(self):
        self.log.info('CLOSING SOCKET')
        self.s.close()

