import socket
import os
import logging
import timeit


class Connection(object):
    def __init__(self, HOST='' , PORT= 57000):
        while True:
            print " OPENING SOCKET"
            try:
                self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.HOST = HOST
                self.PORT = PORT
                self.connected = False
                self.log = logging.getLogger("vc_logger")
                break
            except socket.error as e:
                self.log.debug('SOCKET ERROR: ' + str(e))
            except socket.herror as e:
                self.log.debug('SOCKET HERROR: ' + str(e))
            except socket.gaierror as e:
                self.log.debug('SOCKET HERROR: ' + str(e))
            except socket.timeout as e:
                self.log.debug('SOCKET HERROR: ' + str(e))
            except Exception as e:
                self.log.debug('SOCKET HERROR: ' + str(e))
    def destroy(self):
        self.s.close()


class Server(Connection):
    def __init__(self, HOST, PORT):
        super(Server, self).__init__(HOST, PORT)
        try:
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except socket.error as e:
            self.log.debug('SOCKET ERROR (SETTING SOCKET OPTIONS): ' + str(e))
            print "Socket Erro"
        except socket.herror as e:
            self.log.debug('SOCKET HERROR (SETTING SOCKET OPTIONS): ' + str(e))
            print "Socket Erro"
        except socket.gaierror as e:
            self.log.debug('SOCKET GAIERROR (SETTING SOCKET OPTIONS): ' + str(e))
            print "Socket Erro"
        except socket.timeout as e:
            self.log.debug('SOCKET TIMEOUT (SETTING SOCKET OPTIONS): ' + str(e))
            print "Socket Erro"
        except Exception as e:
            self.log.debug('ERROR SETTING SOCKET OPTIONS: ' + str(e))
            print "Socket Erro"

    def connect(self, connections=1):

        attempts = 0
        while True:
            try:
                self.s.bind((self.HOST, self.PORT))
                self.s.listen(connections)
                break
            except socket.error as e:
                print "Socket Erro"
                self.log.debug('SOCKET ERROR: ' + str(e))
            except socket.herror as e:
                print "Socket Erro"
                self.log.debug('SOCKET HERROR: ' + str(e))
            except socket.gaierror as e:
                print "Socket Erro"
                self.log.debug('SOCKET GAIERROR: ' + str(e))
            except socket.timeout as e:
                print "Socket Erro"
                self.log.debug('SOCKET TIMEOUT: ' + str(e))
            except KeyboardInterrupt:
                raise
            except Exception as e:
                print "Socket Erro"
                self.log.debug('ERROR BINDING SOCKET: ' + str(e))

            print 'Attempts: ' + str(attempts)
            attempts += 1

    def accept(self):
        self.conn, self.addr = self.s.accept()
        self.connected = True


    def disconnect(self):
        try:
            if self.connected:
                self.conn.close()
        except Exception as e:
            print "Erro desconectar"
            self.log.debug('ERROR CLOSING CONNECTION: ' + str(e))

    def send_message(self, message=''):

        try:
            self.conn.sendall(message)
        except IOError as e:
            print "Socket Erro"
            self.log.debug('ERROR: ' + str(e))
        except socket.error as e:
            print "Socket Erro"
            self.log.debug('SOCKET ERROR: ' + str(e))
        except socket.herror as e:
            print "Socket Erro"
            self.log.debug('SOCKET HERROR: ' + str(e))
        except socket.gaierror as e:
            print "Socket Erro"
            self.log.debug('SOCKET GAIERROR: ' + str(e))
        except socket.timeout as e:
            print "Socket Erro"
            self.log.debug('SOCKET TIMEOUT: ' + str(e))
        except KeyboardInterrupt:
            print "Socket Erro"
            self.log.debug('Finished')
        else:
            print "Send...."
            self.log.debug('Finished sending')

    def receive_message(self, nbytes = 1024):
        data = self.conn.recv(nbytes)

        if not data:
            self.log.debug('ERROR: Connection was closed on the other side')
            self.destroy()
        #print data

        return data

    def destroy(self):
        try:
            self.disconnect()
            self.s.close()
        except Exception as e:
            print "erro disconnect"
            self.log.debug('ERROR CLOSING SOCKET: ' + str(e))



if __name__ == '__main__':
    try:
        s = Server()
        s.connect()
        s.accept()
        start = timeit.default_timer()
        s.receive_message()
        end = timeit.default_timer()

        print end - start
    except Exception as e:
        s.destroy()
