# New File
import socket
import connection
import timeit
from threading import Thread
import json, requests
end = False

''' 
    def to receive data from the Actuator and Send it to the    
    
 '''
def time_rec():
    
    # Open Actuator Conection
    a = connection.Server('', 59000)
    a.connect()
    a.accept()


    while True:
        if end:
            break
        time_command_recv  = a.receive_message()
        if not time_command_recv:
            break
        time_command_recv_str = str(time_command_recv)
        time_start_split = time_command_recv_str.split()
        time_start = time_start_split[0]
        command_recv = time_start_split[1]
        time_finaly_proc = time_start_split[2]
               

        time_end = timeit.default_timer()
        total_time = time_end - float(time_start)
        print('--------------------')
        print(time_start) 
        print(command_recv)
        print(float(time_finaly_proc)) 
        print(float(total_time))
        print(time_end) 
        print(float(time_start))
        
        # Send data for ColiSEU 
        dados_coliseu = data={"command": str(command_recv), "time": str(total_time), "time_proc": str(time_finaly_proc),"user": "UserA", "tier": "vmserver", "technology":"sign"}
        response = requests.post("http://coliseu.inf.ufrgs.br/rest/save.php", data=dados_coliseu)
        print response.status_code
        print response.content
        print "Comando Coliseu"

    # Close Actuator connection 
    a.destroy()

    print "End of time_rec"



def main():
    global end
    global HOSTS
    
    # VM Bristol / Vm Server / Vm Local
    HOSTS = '137.222.204.77'
   
    # Conexao Sensor - VM
    c = connection.Client(HOSTS, 5006)
    c.connect()

    # Conexao com o PC
    s = connection.Server('', 47000)
    s.connect()
    s.accept()

    t = Thread(target=time_rec)
    t.start()


    try:
        #conn, addr = s.accept()
        while True:
            dados = s.receive_message()
            resultDados = str(dados)
            splitresult = resultDados.split()
            extends = splitresult[0]
            hand_left = splitresult[1]
            hand_right = splitresult[2]
            clock = splitresult[4]
            palm = splitresult[5]
            time_send = timeit.default_timer()

            # print "Extends " + extends
            # print "Hand_left" + hand_left
            # print "Hand Right" + hand_right
            
            # print "Time Send " + str(time_send)

            #print "Time Actuator: " + a.receive_message()

            send_date = str(extends) + " " + str(hand_left) + " " + str(hand_right) + " " + str(clock) + " " + str(time_send) + " " + str(palm) + " "
            c.send_message(send_date)           

    except KeyboardInterrupt:
        end = True
        print "Finished"
    finally:
        c.destroy()
if __name__ == "__main__":
    main()

