import socket
import connection
import timeit
from threading import Thread

end = False

def time_rec():
    # Server Actuator
    a = connection.Server('', 59000)
    a.connect()
    a.accept()

    while True:
        if end:
            break
        time_start = a.receive_message()

        if not time_start:
            break
        time_end = timeit.default_timer()
        total_time = time_end - float(time_start)
        print('Total time: ' + str(total_time))

    a.close()

    print "End of time_rec"



def main():
    global end
    # HOSTS = '192.168.0.84'
    # PORTS = 59000
    #
    # HOST = ''
    # PORT = 47000
    # s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # s.bind((HOST, PORT))
    # s.listen(2)
    #
    # c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # c.connect((HOSTS, PORTS))

    c = connection.Client('192.168.0.1', 32000)
    c.connect()

    # Server Main
    s = connection.Server('', 47000)
    s.connect()
    s.accept()

    t = Thread(target=time_rec)
    t.start()


    try:
        #conn, addr = s.accept()
        while True:
            dados = s.receive_message()
            resultDados = str(dados)
            splitresult = resultDados.split()
            extends = splitresult[0]
            hand_left = splitresult[1]
            hand_right = splitresult[2]
            clock = splitresult[4]
            palm = splitresult[5]
            time_send = timeit.default_timer()

            # print "Extends " + extends
            # print "Hand_left" + hand_left
            # print "Hand Right" + hand_right
            # print "Clock " + clock
            # print "Time Send " + str(time_send)

            #print "Time Actuator: " + a.receive_message()

            send_date = str(extends) + " " + str(hand_left) + " " + str(hand_right) + " " + str(clock) + " " + str(time_send) + " " + str(palm) + " "
            c.send_message(send_date)

    except KeyboardInterrupt:
        end = True
        print "Finished"
    finally:
        c.close()
if __name__ == "__main__":
    main()