import os, sys, inspect, thread, time
import socket

#  LeapMotion
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
# Windows and Linux
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
# Mac
#arch_dir = os.path.abspath(os.path.join(src_dir, '../lib'))

sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))

import Leap

# Conexao com o Sensor
HOST = '192.168.0.39'
PORT = 47000

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))


class time_life_hand():
    def __init__(self):
        self.time_life = 0

    def setTime(self, time):
        self.time_life = time

    def getTime(self):
        return self.time_life



class SampleListener(Leap.Listener):

    def on_connect(self, controller):
        print "Connected"

    def on_frame(self, controller):

        frame = controller.frame()
        print "Frame id: %d, timestamp: %d, hands: %d, fingers: %d" % (
            frame.id, frame.timestamp, len(frame.hands), len(frame.fingers))
        # Verifica quandos dedos estao extendidos

        # Enable Gesture
        controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE)

        hand = frame.hands.leftmost
        hand_palm_center = hand.palm_position
        
        result_hand_palm = str(hand_palm_center)
        split_result_pal = result_hand_palm.split("(")

        hand_palm = split_result_pal[1]
        r = hand_palm
        resultr = r.split(",")
        sendPaml = resultr[0]
        
        print sendPaml

        if hand.is_valid:
            # Data hand left
            hand_name_left = "L" if hand.is_left else "R"
            # Data hand right
            hand_name_right = "R" if hand.is_right else "L"
            
            progressGesture = 0
            clockwiseness = 0




            # Gesture
            for gesture in frame.gestures():
                if gesture.is_valid:
                    if gesture.type == Leap.Gesture.TYPE_SWIPE:
                        swipe = Leap.SwipeGesture(gesture)
                        print "TYPE_SWIPE------------------------------------------------------"
                        print  "Swipe id: %d,  position: %s, direction: %s, speed: %f" % (
                            gesture.id,
                            swipe.position, swipe.direction, swipe.speed)
                    if gesture.type == Leap.Gesture.TYPE_CIRCLE:
                        circle = Leap.CircleGesture(gesture)

                        # Determine clock direction using the angle between the pointable and the circle normal
                        if circle.pointable.direction.angle_to(circle.normal) <= Leap.PI / 2:
                            
                            clockwiseness = "D"
                            
                        else:

                            clockwiseness = "E"
                            


                        # Calculate the angle swept since the last frame
                        swept_angle = 0
                        countGesture = 0
                        if circle.state != Leap.Gesture.STATE_START:
                            previous_update = Leap.CircleGesture(controller.frame(1).gesture(circle.id))
                            swept_angle = (circle.progress - previous_update.progress) * 2 * Leap.PI
                            countGesture = countGesture + 1
                            print "  Circle id: %d, progress: %1.0f, radius: %f, angle: %f degrees, %s" % (
                                gesture.id, circle.progress, circle.radius, swept_angle * Leap.RAD_TO_DEG,
                                clockwiseness)
                        progressGesture = circle.progress
                    print "count gesture"
                    print countGesture
#                if countGesture == 1:
#                    circle.state = circle.STATE_STOP


        else:
            hand_name_left = 0
            hand_name_right = 0
            progressGesture = 0
            clockwiseness = 0



        print clockwiseness

        # Count how many extended fingers
        getCounthands = len(frame.hands)
        extends =  len(frame.fingers.extended())
        # Send to socket
        # 0 Extended fingers
        # 1 Data Hand Left
        # 2 Data Hand Right
        # 3 Gesture progress
        # 4 clockwise and counterclockwise
        # 5 Data palm - Postion X


        sendString = str(extends)+ " " + str(hand_name_left) + " " + str(hand_name_right) + " " + str(progressGesture) + " " + str(clockwiseness) + " " + str(sendPaml) +  " "
        s.send(sendString)
        print " Extended Fingers\n\n"
        print extends
        print " --------- Hand -------\n\n"
        print getCounthands
        print hand_name_left
        print hand_name_right


def main():
    # Create a sample listener and controller

    listener = SampleListener()


    controller = Leap.Controller()

    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    print ("Extendes")
    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        print("dsfads")
        sys.stdin.readline()

    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)

if __name__ == "__main__":
    main()
