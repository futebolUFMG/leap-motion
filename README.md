# Sign Control Readme #
## Installation and Usage ##
### Dependencies ###

#### PC ####

> Leapd SDK 
>> Download on : https://developer.leapmotion.com/sdk/v2
>>
>> Install the Leap Motion package 
>>
>> 64-bit Ubuntu:
>>
>> sudo dpkg --install Leap-*-x64.deb
>>
>> 32-bit Ubuntu:
>>
>> sudo dpkg --install Leap-*-x86.deb

### Sensor ###
> sudo apt-get install python-pip  
> sudo pip install --upgrade pip  
> sudo apt-get install python-dev 

### Actuator ###
> sudo apt-get install python-pip  
> sudo pip install --upgrade pip  
> sudo pip install wheel  
> sudo pip install beautifulhue  

### VM ###
> sudo apt-get install python-pip  
> sudo pip install --upgrade pip  
> sudo apt-get install python-dev 

###Usage
1. Clone the repository
> git clone https://futebolUFMG@bitbucket.org/futebolUFMG/leap-motion.git

2. Copy the files to each agent
> cd leapmotion  
> scp -r pc **_USER_**@**_IP_LeapMotion_Computer_**:~  
> scp -r sensor pi@**_IP_RASPBERRY_SENSOR_**:~  
> scp -r vm **_USER_**@**_IP_VM_**:~  
> scp -r actuator pi@**_IP_RASPBERRY_ACTUATOR_**:~